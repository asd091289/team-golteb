module.exports = (io)=>{
    const Feed = require('../model/feed-model.js');
 
    io.on('connect',socket=>{

        socket.on('sendArticle', data => {

           const feed = new Feed({
            body : data.body,
            title : data.title,
            isPrivate : data.isPrivate
            
           });
        
            feed.save().then(res => {
                io.sockets.emit('sendArticle', res);
            }).catch(err => {
                console.log(err);
            });


        });


        socket.on('findAll', () => {

            Feed.find().then(res => {
         
                io.sockets.emit('findAll', res);
            }).catch(err => {
                console.log(err);
            });
        });

        socket.on('findAllPublic', () => {

            Feed.find().where('isPrivate').equals(false).then(res => {
         
                io.sockets.emit('findAllPublic', res);
            }).catch(err => {
                console.log(err);
            });
        });

    })

}
