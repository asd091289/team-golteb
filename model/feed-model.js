const mongoose = require('mongoose');

const Feed = mongoose.Schema({
    body: String,
    title: String,
    isPrivate : Boolean
}, {
    timestamps: true
});

module.exports = mongoose.model('Feed', Feed);