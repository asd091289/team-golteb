const express = require('express');
const socket = require('socket.io');
const bodyParser = require('body-parser');
const dbConfig = require('./config/config.js');
const mongoose = require('mongoose')

mongoose.Promise = global.Promise;
var port = process.env.PORT || 8070;
// Connecting to the database
mongoose.connect(dbConfig.url, {
    useNewUrlParser: true
}).then(() => {
    console.log("Successfully connected to the database");    
}).catch(err => {
    console.log('Could not connect to the database. Exiting now...', err);
    process.exit();
});


const app = express();

app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());


const server = app.listen(port, ()=>{
    console.log('listening for requests on port ' + port);
});

app.use(express.static(__dirname +'/web'));
app.get('*', function(req, res) {
  res.sendFile(__dirname + '/web/index.html')
})


app.use((req,res,next)=>{ //allow cros
    res.header('Access-Control-Allow-Origin','*');
    res.header('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept, Authorization');
    if(req.method === 'OPTIONS'){
        res.header('Access-Control-Allow-Methods','PUT, GET, POST, DELETE');
        return res.status(200).json({});
    }
    next();
})

require('./routes.js')(app);


const io = socket(server);
require('./resource/feedresource')(io);
